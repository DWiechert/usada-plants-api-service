# --- !Ups

create table "plants" as select * from CSVREAD('c:\\usda_plants_20160713.csv');

# --- !Downs

drop table "plants" if exists;