#!/bin/sh

export APPLICATION_SECRET=$(head -c 32 /dev/urandom | base64)
sudo docker run -d -t -i -e APPLICATION_SECRET="${APPLICATION_SECRET}"