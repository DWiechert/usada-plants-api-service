FROM openjdk:8-jre
COPY service /service
EXPOSE 9000 9443
CMD /service/bin/start -Dhttps.port=9443 -Dplay.crypto.secret=secret