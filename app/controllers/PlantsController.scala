package controllers

import javax.inject.Inject
import models.PlantsRepository
import play.api.libs.json.Json
import play.api.mvc.{MessagesAbstractController, MessagesControllerComponents}

import scala.concurrent.ExecutionContext

class PlantsController @Inject()(repo: PlantsRepository,
                                 cc: MessagesControllerComponents
                                )(implicit ec: ExecutionContext)
	extends MessagesAbstractController(cc) {

//	def getPlants = Action.async { implicit request =>
//		repo.list().map { plants =>
//			Ok(Json.toJson(plants))
//		}
//	}
}