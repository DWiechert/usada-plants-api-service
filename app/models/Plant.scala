package models

import play.api.libs.json._

case class Plant(
	                acceptedSymbol: String,
	                synonymSymbol: String,
	                symbol: String,
	                scientificName: String,
	                hybridGenusIndicator: String,
	                genus: String,
	                hybridSpeciesIndicator: String,
	                species: String,
	                subspeciesPrefix: String,
	                hybridSubspeciesIndicator: String,
	                variety: String,
	                subvarietyPrefix: String,
	                subvariety: String,
	                formaPrefix: String,
	                forma: String,
	                generaBinomialAuthor: String,
	                trinomialAuthor: String,
	                quadranomialAuthor: String,
	                questionableTaxonIndicator: String,
	                parent: String,
	                commonName: String,
	                plantsFloristicArea: String,
	                category: String,
	                genus1: String,
	                family: String,
	                familySymbol: String,
	                familyCommonName: String,
	                order: String,
	                subclass: String,
	                klass: String,
	                subdivision: String,
	                division: String,
	                superdivision: String,
	                subkingdom: String,
	                kingdom: String,
	                itisTsn: String,
	                duration: String,
	                growthHabit: String,
	                nativeStatus: String,
	                federalNoxiousStatus: String,
	                federalNoxiousCommonName: String,
	                stateNoxiousStatus: String,
	                stateNoxiousCommonName: String,
	                invasive: String,
	                federalTEStatus: String,
	                stateTEStatus: String,
	                stateTECommonName: String,
	                nationalWetlandIndicatorStatus: String,
	                regionalWetlandIndicatorStatus: String
                )

object Plant {
	implicit val plantFormat = Json.format[Plant]
}